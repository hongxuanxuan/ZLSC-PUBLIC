// @ts-ignore
import React from 'react'
// @ts-ignore
import ReactDOM from 'react-dom'
import './index.less'
import reportWebVitals from './reportWebVitals'
// @ts-ignore
import Zlsc from 'Pages/zlsc'

ReactDOM.render(
    <React.StrictMode>
        <Zlsc />
    </React.StrictMode>,
    document.getElementById('root')
)
reportWebVitals();
