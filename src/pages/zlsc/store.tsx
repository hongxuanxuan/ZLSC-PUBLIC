import React from 'react'
import { observable, action, runInAction } from 'mobx'
// @ts-ignore
import { post, get } from 'Utils/request'
// @ts-ignore
import ApiPath from 'Public/apiPath'
// @ts-ignore
import { CryptoJSTripleDES } from 'Utils/Utils'

class Store {
    @observable zlxx: any = {}
    @action setZlxx = (v: any): void => {
        this.zlxx = v
    }
    decrypt = (str: string) => CryptoJSTripleDES.decrypt(str)
    @action.bound getZlxx = async (zhiling: string) => {
        const djm = await this.getJson(`zlparams/${zhiling}`, 'string')
        this.setZlxx(JSON.parse(this.decrypt(djm)))
    }
    @action.bound getJson = async (name: string, type?:string) => {
        try {
            const res = await get(`${ApiPath.ZLSC_GET_LIST}${name}.json`, {}, {}, type)
            if (res.data) {
                return res.data
            }
        } catch {
            console.log('请求失败')
            return []
        }
    }
    @observable zlData: any = []
    @action setZlData = (v: any): void => {
        this.zlData = v
    }
    /**
     * 指令查询
     */
    @action.bound getZlData = async () => {
        try {
            const res = await get(`${ApiPath.ZLSC_GET_LIST}zhilingList.json`, {})
            runInAction(() => {
                this.setZlData(res.data || [])
            })
        } catch (e) {
            this.setZlData([])
        }
    }
}
export default new Store()
