import React from 'react'
import {
    Button,
    SearchBar,
    Form,
    Input,
    List,
    InfiniteScroll,
    Toast,
    NavBar,
    Dialog,
    Popover,
    Selector,
    CapsuleTabs,
    NumberKeyboard,
    Divider
} from 'antd-mobile'
// @ts-ignore
import SelectZl from 'Components/select/SelectZl'
// @ts-ignore
import copy from 'copy-to-clipboard'
// @ts-ignore
import { showLoading } from 'Utils/Utils'
import { DownOutline, SearchOutline, UserAddOutline } from 'antd-mobile-icons'
// @ts-ignore
import XModal from 'Components/XModal'
import Store from './store'
import moment from 'moment'
import './zlsc.less'

const Item = Form.Item
interface itemParams {
    value: string
    label: string
}

class Zlsc extends React.Component<any, any> {
    formRef: React.RefObject<any>
    ysJson: any = {}

    constructor(props: any) {
        super(props)
        this.state = {
            bunStyle: {}, // 提交按钮样式
            sjxzVisible: '', // 数据选择页是否显示
            zlData: [], // 指令列表
            searchVal: {}, // 查询筛选Val
            zlxzVisible: '', // 指令选择是否显示
            kqdz: {}, // 考勤地址信息
            zlxx: <>未选择指令</>,
            numberKeyVisible: false, // 打开数字键盘
            showData: [], // 筛选框显示的内容
            showDataType: '', // 筛选框显示的类型
            showDataLoad: {
                page: 1,
                size: 100,
                hasMore: true,
            },
            tabsKey: '',
        }
        this.formRef = React.createRef()
    }
    async componentDidMount() {
        const winInnHeight = window.innerHeight
        const bodyHeight = document.body.clientHeight
        if (winInnHeight > bodyHeight) {
            this.setState({
                bunStyle: {
                    position: 'fixed',
                    width: `calc(100% - 24px)`,
                    bottom: '3px',
                    left: '12px'
                }
            })
        }
        this.showLoading('加载中')
        await this.loadZl()
        if (this.state.kqdz.val) {
            this.getZlxx(this.state.kqdz.val.bm || this.state.kqdz.val)
        }
        Toast.clear()
    }

    /**
     * 获取指令信息
     */
    getZlxx = async (zhiling: string) => {
        await Store.getZlxx(zhiling)
        let zlxx: React.ReactElement
        if (Store.zlxx && Store.zlxx.length > 0) {
            this.setState({
                tabsKey: Store.zlxx[0].key
            })
            zlxx = Store.zlxx.length > 1 ? <CapsuleTabs
                onChange={(e: any) => {
                    this.numberKeyChange('close')
                    this.setState({
                        tabsKey: e
                    })
                }}
                >
                {Store.zlxx.map((item: any) => {
                    return <CapsuleTabs.Tab title={item.mc} key={item.key}>
                        {this.getZlxxParam(item.params)}
                    </CapsuleTabs.Tab>
                })}
            </CapsuleTabs> : this.getZlxxParam(Store.zlxx[0].params)
        } else {
            zlxx = <p style={{ textAlign: 'center' }}>此命令无参数</p>
        }
        this.setState({
            zlxx
        })
    }
    getJson = async (key: string) => {
        this.showLoading()
        const res = await Store.getJson(key)
        Toast.clear()
        return res
    }
    fwvalidator = (v: any, val: any, param: any) => {
        return new Promise(((resolve, reject) => {
            if (!val) {
                resolve(null)
            }
            if (param.max && param.max < val) {
                reject('超过最大限制')
            }
            if (param.min && param.min > val) {
                reject('低于最小限制')
            }
            resolve(null)
        }))
    }
    getZlxxParam = (params: any) => {
        return <>{params.map((item: any, index: number) => {
            let JSXDOM: any = <Input/>
            let click: Function|undefined
            switch (item.type) {
                case 'number':
                    click = () => {
                        this.setState({
                            numberKeyVisible: item.key
                        })
                    }
                    JSXDOM = <Input readOnly placeholder={`请输入${item.name}`} />
                    break
                case 'string':
                    JSXDOM = <Input readOnly={item.required} placeholder={`请输入${item.name}`} />
                    break
                case 'select':
                    JSXDOM = <Selector
                        options={item.data}
                        multiple={item.radio === 'check'}
                    />
                    break
                default:
                    click = async () => {
                        if (!this.ysJson[item.type]) {
                            this.ysJson[item.type] = await this.getJson(item.type)
                        }
                        let showData: any = []
                        let inx = 0
                        // this.ysJson[item.type].forEach((item: any) => {
                        //     if (inx === 100) return
                        //     const data = {
                        //         name: item.name,
                        //         data: []
                        //     }
                        //     item.data.forEach((item2: any) => {
                        //         if (inx === 100) return
                        //         // @ts-ignore
                        //         data.data.push(item2)
                        //         inx = inx + 1
                        //     })
                        //     showData.push(data)
                        // })
                        this.setState({
                            sjxzVisible: item.key,
                            showDataType: item.type,
                            showData: [],
                            showDataLoad: { ...this.state.showDataLoad, page: 1, hasMore: true }
                        }, this.showDataLoad)
                    }
                    this.setState({
                        [item.key]: item.default
                    })
                    JSXDOM = <SelectZl placeholder={`选择${item.name}`}/>

            }
            const rules: any = [
                {
                    required: item.required, message: `请输入${item.name}`
                }
            ]
            rules.push({
                validator: (field: any, value: any) => this.fwvalidator(field, value, item)
            })
            return click ? (
                <Item
                    label={`${item.name}：`}
                    name={item.key}
                    key={item.key}
                    // @ts-ignore
                    onClick={click}
                    initialValue={item.default}
                    rules={rules}
                >
                    {JSXDOM}
                </Item>
            ) : (
                <Item
                    label={`${item.name}：`}
                    name={item.key}
                    key={item.key}
                    initialValue={item.default}
                    rules={rules}
                >
                    {JSXDOM}
                </Item>
            )
        })}</>
    }
    loadZl = async () => {
        this.showLoading()
        await Store.getZlData().then(() => {
            Toast.clear()
            this.setState({
                zlData: Store.zlData,
                // @ts-ignore
                kqdz: Store.zlData.length > 0 && Store.zlData[0]
            })
        })
    }
    /**
     * 加载中
     * @param v
     * @param time
     */
    showLoading = (v: string = '加载中', time: number = 0) => {
        Toast.show({
            content: v,
            icon: 'loading',
            duration: time
        })
    }
    /**
     * 指令选择
     */
    zlxz = (item: any) => {
        this.setState({
            // @ts-ignore
            kqdz: item,
            zlxzVisible: false
        })
        this.showLoading()
        this.getZlxx(item.bm || item.val).then(() => {
            Toast.clear()
        })
    }
    /**
     * 搜索框值改变
     * @param val
     */
    kqrsearchValChange = (val: string|undefined, key: string) => {
        // 有限制搜索人姓名时不可以修改
        this.setState({
            searchVal: { ...this.state.searchVal, [key]: val },
            showDataLoad: { ...this.state.showDataLoad, page: 1, hasMore: true },
        }, this.showDataLoad)
    }
    /**
     * 点击考勤地址选择
     */
    clickKqdz = (key: string| boolean) => {
        this.numberKeyChange('close')
        this.setState({
            zlxzVisible: key
        })
    }
    /**
     * 1 v.value
     * 2 x+v
     * 3 lv+v
     * 4 r+v
     * 5 v[0].value
     * @param v
     * @param lx
     */
    zlRender = (lx: number) => {
        let fun: Function
        switch (lx) {
            case 1:
                fun = (v: itemParams) => {
                    return v.value
                }
                break
            case 2:
                fun = (v: number|string) => {
                    return `x${v}`
                }
                break
            case 3:
                fun = (v: number|string) => {
                    return `lv${v}`
                }
                break
            case 4:
                fun = (v: number|string) => {
                    return `r${v}`
                }
                break
            case 5:
                fun = (v: any) => {
                    return v && v[0].value || ''
                }
                break
            case 6:
                fun = (v: number|string) => {
                    return `@${v}`
                }
                break
            default:
                fun = (v: any) => v
        }
        return fun
    }
    // 指令生成
    zlsc = () => {
        const { tabsKey, kqdz } = this.state
        if (!Store.zlxx || Store.zlxx.length === 0) {
            this.copyStr(`/${kqdz.val}`)
            return
        }
        const { validateFields } = this.formRef.current
        const zlsj = Store.zlxx.find((item: any) => {
            return item.key === tabsKey
        })
        if (zlsj) {
            const zlParamsKey = zlsj.params.map((param: any) => param.key)
            let zlpj: any = `/${kqdz.val}`
            validateFields(zlParamsKey).then((val: any) => {
                let checkKey = ''
                zlsj.params.forEach((zl: any) => {
                    if (zl.radio === 'check' && !checkKey) {
                        checkKey = zl.key
                        zlpj = `${zlpj} ?`
                    } else {
                        if (!val[zl.key]) return
                        const paramVal = zl.render ? this.zlRender(zl.render)(val[zl.key]) : val[zl.key]
                        zlpj = `${zlpj}${zl.jgf || ' '}${paramVal}`
                    }
                })
                if (checkKey) {
                    const chzl = zlsj.params.find((item: any) => item.key === checkKey)
                    zlpj = val[checkKey].map((checkData: any) => {
                        return zlpj.replace(' ?', `${chzl.fgf || ' '}${chzl.render ? this.zlRender(chzl.render)(checkData) : checkData}`)
                    })
                }
                const config = {
                    confirmText: '复制指令',
                    content: (<div style={{ maxHeight: '200px' }}>
                        {typeof zlpj === 'string' ? zlpj : zlpj.map((d: string, index: number) => <>
                            {index + 1}：
                            <span style={{ border: 'black 1px dashed', borderRadius: '0.3em' }}>{d}</span><br/>
                        </>)}
                    </div>),
                    onConfirm: () => {
                        let zlCopy = ''
                        if (typeof zlpj === 'string') {
                            zlCopy = zlpj
                        } else {
                            zlpj.forEach((d: string, inx: number) => {
                                zlCopy = `${zlCopy}${inx !== 0 ? '\n' : ''}${d}`
                            })
                        }
                        this.copyStr(zlCopy)
                    }
                }
                Dialog.confirm(config)
            })
        }
    }
    /**
     * 复制字符串
     * @param str
     */
    copyStr = (str: string) => {
        if (copy(str)) {
            Toast.show({
                icon: 'success',
                content: '复制成功'
            })
        } else {
            Toast.show({
                icon: 'fail',
                content: '复制失败'
            })
        }
    }
    /**
     * 更多选择
     */
    leftPopoverClick = (node: any) => {
        switch (node.key) {
            case 'fhjb':
                window.open('http://60.205.200.253:3001/zlsc')
                break
            case 'xmgs':
                this.joinQQ({
                    url: 'https://jq.qq.com/?_wv=1027&k=NITDZlQA',
                    ms: 'xXmTnT.Genshin私服1群',
                    bm: '864972412',
                })
                break
            case 'xmgs2':
                this.joinQQ({
                    url: 'https://jq.qq.com/?_wv=1027&k=usieYQxV',
                    ms: 'xXmTnT.Genshin私服2群',
                    bm: '648101309',
                })
                break
            case 'zlfk':
                this.joinQQ({
                    url: 'https://jq.qq.com/?_wv=1027&k=IsE8x5vS',
                    ms: '指令问题反馈总群，本群不提供任何私服连接，仅解决指令问题',
                    bm: '207151722',
                })
                break
            default:
                console.log('无操作')
        }
    }
    joinQQ = (params: { url: string, ms: string|React.ReactElement, bm: string }) => {
        const config = {
            confirmText: '点击加入',
            content: (<div style={{ maxHeight: '200px' }}>
                群号码：{params.bm}<br/>
                {params.ms}
            </div>),
            closeOnMaskClick: true,
            onConfirm: () => {
                window.open(params.url)
            }
        }
        Dialog.alert(config)
    }
    numberKeyChange = (type: string, num?: string| number) => {
        if (type === 'close') {
            this.setState({
                numberKeyVisible: false
            })
            return
        }
        const { setFieldsValue, getFieldValue } = this.formRef.current
        let val = getFieldValue(this.state.numberKeyVisible) || ''
        if (type === 'delete') {
            val = val.substring(0, val.length - 1)
        } else {
            val = `${val}${num}`
        }
        setFieldsValue({
            [this.state.numberKeyVisible]: val
        })
    }
    showDataLoadTimeOut: any
    showDataLoad = () => {
        if (this.showDataLoadTimeOut) {
            clearTimeout(this.showDataLoadTimeOut)
        }
        this.showDataLoadTimeOut = setTimeout(() => {
            const {showDataLoad, showDataType, sjxzVisible, searchVal} = this.state
            const loadSize = showDataLoad.page * showDataLoad.size
            const arr: { name: any; data: never[] }[] = []
            let inx = 0
            this.ysJson[showDataType].forEach((item: any, index1: number) => {
                if (inx === loadSize) return
                const data = {
                    name: item.name,
                    data: []
                }
                const isend = index1 === this.ysJson[showDataType].length - 1
                const itemLen = item.data.length
                item.data.forEach((item2: itemParams, index2: number) => {
                    if (inx === loadSize) return
                    if (searchVal[sjxzVisible]) {
                        if (item2.label.includes(searchVal[sjxzVisible]) || item2.value.includes(searchVal[sjxzVisible])) {
                            // @ts-ignore
                            data.data.push(item2)
                            inx = inx + 1
                        }
                    } else {
                        // @ts-ignore
                        data.data.push(item2)
                        inx = inx + 1
                    }
                    if (isend && index2 === itemLen - 1) {
                        this.setState({
                            showDataLoad: {...this.state.showDataLoad, hasMore: false}
                        })
                    }
                })
                data.data.length > 0 && arr.push(data)
            })
            this.showDataLoadTimeOut = undefined
            this.setState({
                showData: arr
            })
        })
    }
    /*
    * 确认选择
    * */
    searchValSelect = (item: any) => {
        const { sjxzVisible } = this.state
        const { setFieldsValue } = this.formRef.current
        setFieldsValue({
            [sjxzVisible]: [item]
        })
        this.setState({
            sjxzVisible: ''
        })
    }
    render() {
        const { numberKeyVisible, zlxzVisible, searchVal, sjxzVisible, zlxx, showData, showDataLoad } = this.state
        return (
            <div className={'zlsc'}>
                <NavBar
                    backArrow={''}
                    style={{
                        background: '#fff'
                    }}
                    left={<Popover.Menu
                        actions={[
                            { text: '返回旧版', icon: <SearchOutline className={'leftPopoverIcon'} />, key: 'fhjb' },
                            { text: '指令反馈群', icon: <UserAddOutline className={'leftPopoverIcon'} />, key: 'zlfk' },
                            { text: 'xXmMTnT1群', icon: <UserAddOutline className={'leftPopoverIcon'} />, key: 'xmgs' },
                            { text: 'xXmMTnT2群', icon: <UserAddOutline className={'leftPopoverIcon'} />, key: 'xmgs2' },
                        ]}
                        onAction={node => this.leftPopoverClick(node)}
                        placement='bottomRight'
                        trigger='click'
                    ><a>更多<DownOutline /></a></Popover.Menu>}
                    right={<><a onClick={() => this.clickKqdz('zhilingList')}>切换指令</a>:{this.state.kqdz.val}</>}
                />
                <Form
                    ref={this.formRef}
                    mode={'card'}
                    layout={'horizontal'}
                >
                    {zlxx}
                    <br/>
                    <br/>
                    <Button
                        block
                        style={this.state.bunStyle}
                        color='primary'
                        size='large'
                        onClick={this.zlsc}
                    >{Store.zlxx && Store.zlxx.length > 0 ? '生成指令' : '复制指令'}</Button>
                </Form>
                <XModal
                    style={{}}
                    showNavBar
                    navBarConfig={{
                        onBack: () => this.setState({ sjxzVisible: false })
                    }}
                    title={'选择'}
                    visible={sjxzVisible}
                >
                    <SearchBar
                        placeholder={'请输入姓名筛选'}
                        maxLength={10}
                        value={searchVal[sjxzVisible]}
                        onChange={(e: any) => this.kqrsearchValChange(e, sjxzVisible)}
                    />
                    <div style={{ maxHeight: 'calc(100vh - 80px)', overflowY: 'auto' }}>
                        <List>
                            {showData.map((item: any) => {
                                const dom = [
                                    <Divider>{item.name}</Divider>
                                ]
                                dom.push(...item.data && item.data.map((item2: itemParams, indx: number) => {
                                    return <List.Item onClick={() => this.searchValSelect(item2)} key={item2.value}>{item2.value}|{item2.label}</List.Item>
                                }))
                                return dom
                            })}
                        </List>
                        <InfiniteScroll loadMore={() => {
                            return new Promise<void>((resolve, reject) => {
                                showDataLoad.hasMore && this.setState({
                                    showDataLoad: { ...this.state.showDataLoad, page: showDataLoad.page + 1 }
                                }, this.showDataLoad)
                                resolve()
                            })
                        }} hasMore={showDataLoad.hasMore} />
                    </div>
                </XModal>
                <XModal
                    style={{}}
                    showNavBar
                    navBarConfig={{
                        onBack: () => this.setState({ zlxzVisible: false })
                    }}
                    title={'指令选择'}
                    visible={zlxzVisible}
                >
                    <SearchBar
                        placeholder={'请输入指令筛选'}
                        maxLength={10}
                        value={searchVal[zlxzVisible]}
                        onChange={e => this.kqrsearchValChange(e, zlxzVisible)}
                    />
                    <List>
                        {this.state.zlData.map((item: any, index: number) => {
                            if (searchVal[zlxzVisible] && !item.label.includes(searchVal[zlxzVisible]) && !item.val.includes(searchVal[zlxzVisible])) return null
                            return <List.Item onClick={() => this.zlxz(item)} key={index}>{item.val}|{item.label}</List.Item>
                        })}
                    </List>
                </XModal>
                <NumberKeyboard
                    visible={numberKeyVisible}
                    onClose={(): void => this.numberKeyChange('close')}
                    onInput={(e: any): void => this.numberKeyChange('input', e)}
                    onDelete={(): void => this.numberKeyChange('delete')}
                />
            </div>
        )
    }
}

export default Zlsc
